<?php


namespace Swoft\Crud\Handlers;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Contract\CrudTableDefinitionObjectInterface;
use Swoft\Crud\Contract\MysqlTableColumnObjectHandlerInterface;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class ControllerColumnObjectHandler
 * @Bean(name="controllerHandler")
 */
class ControllerColumnObjectHandler implements MysqlTableColumnObjectHandlerInterface
{
    private $createTimeField = 'create_time';

    private $updateTimeField = 'update_time';

    /**
     * @var array $usedClasses
     */
    private $usedClasses = [
        "use Swoft\Http\Server\Annotation\Mapping\Controller;",
        "use Swoft\Db\Exception\DbException;",
        "use Swoft\Http\Message\Request;",
        "use Swoft\Http\Server\Annotation\Mapping\RequestMapping;",
        "use Swoft\Http\Server\Annotation\Mapping\RequestMethod;",
        "use Swoft\Validator\Annotation\Mapping\Validate;",
        "use Swoft\Crud\Validator\PaginateValidator;",
    ];

    /**
     * @var array $stubVariables
     */
    private $stubVariables = [];

    /**
     * @var CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     */
    private $crudTableDefinitionObject;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     */
    private $mainTableColumnDefinitions;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     */
    private $foreignTableColumnDefinitions;

    /**
     * @var array $destinationFilePath
     */
    private $destinationFilePath;

    /**
     * @param CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     * @param CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     * @param CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     * @param array $destinationFilePath
     * @return array
     * @example
     *  [
     *      'filePath'  =>  'fileContent'
     *  ]
     */
    public function handle(CrudTableDefinitionObjectInterface $crudTableDefinitionObject, array $mainTableColumnDefinitions, array $foreignTableColumnDefinitions, array $destinationFilePath): array
    {
        $this->crudTableDefinitionObject = $crudTableDefinitionObject;
        $this->mainTableColumnDefinitions = $mainTableColumnDefinitions;
        $this->foreignTableColumnDefinitions = $foreignTableColumnDefinitions;
        $this->destinationFilePath = $destinationFilePath;

        return [$destinationFilePath['destinationFiles']['controllerFile']=>$this->createControllerFileContent()];
    }

    private function createControllerFileContent()
    {
        $this->initVariables();
        return $this->replaceContentVariables($this->getStubContent());
    }

    private function getStubContent():string
    {
        return file_get_contents(\Swoft::getAlias('@crud/Stubs/Controller.stub'));
    }

    private function replaceContentVariables(string $content): string
    {
        foreach ($this->stubVariables as $variable => $value)
        {
            $content = str_replace($variable,$value,$content);
        }
        return $content;
    }

    private function initVariables()
    {
        $this->stubVariables['{%createDate%}'] = date('Y-m-d H:i:s');
        $this->stubVariables['{%controllerNamespace%}'] = $this->getControllerNamespace();
        $this->stubVariables['{%controllerName%}'] = $this->mainTableColumnDefinitions['tableInfo']['largeCamelName'];
        $this->stubVariables['{%controllerLowerName%}'] = $this->mainTableColumnDefinitions['tableInfo']['smallCamelName'];
        list(
            $this->stubVariables['{%countOperation%}'],
            $this->stubVariables['{%idsOperation%}'],
            $this->stubVariables['{%dataOperation%}']
            ) = $this->getCountIdsDataOperation();
        $this->stubVariables['{%fields%}'] = $this->getFields();
        $this->stubVariables['{%createTimeField%}'] = $this->createTimeField;
        $this->stubVariables['{%updateTimeField%}'] = $this->updateTimeField;

        //最后再执行这一步
        $this->usedClasses[] = "App\\Validator\\Common\\Admin\\{$this->stubVariables['{%controllerName%}']}Validator;";
        $this->usedClasses[] = "App\\Validator\\Common\\Admin\\NotRequired{$this->stubVariables['{%controllerName%}']}Validator;";
        $this->stubVariables['{%usedClasses%}'] = $this->getUsedClasses();
    }

    private function getControllerNamespace()
    {
        return str_replace('/','\\',ucfirst(mackLtrimPlus($this->destinationFilePath['destinationDirs']['controllerDir'],'@')));
    }

    private function getCountIdsDataOperation()
    {
        $mainTablePrimaryKey = $this->crudTableDefinitionObject->getPrimaryKey()->getColumnName();
        $entityNamespace = $this->getEntityNamespace();
        $this->usedClasses[] = "use {$entityNamespace}\\{$this->mainTableColumnDefinitions['tableInfo']['largeCamelName']};";
        // 如果有关联表
        if ($this->foreignTableColumnDefinitions['tableInfos'])
        {
            $str = "{$this->stubVariables['{%controllerName%}']}::";
            $joinStr = '';
            $fieldStr = "'{$this->mainTableColumnDefinitions['tableInfo']['tableName']}.*'";
            foreach ($this->foreignTableColumnDefinitions['tableInfos'] as $foreignTableName => $tableInfo)
            {
                $joinStr .= "->leftJoin('{$tableInfo['tableName']}','{$this->mainTableColumnDefinitions['tableInfo']['tableName']}.{$tableInfo['relationKey']}','=','{$tableInfo['tableName']}.{$tableInfo['relationPrimaryKey']}')";
                /** @var CrudColumnDefinitionObjectInterface $columnDefinitionObject */
                foreach ($this->foreignTableColumnDefinitions['columnDefinitionObjects'][$foreignTableName] as $columnDefinitionObject)
                {
                    $fieldStr .= ",'{$foreignTableName}.{$columnDefinitionObject->getColumnName()} as {$foreignTableName}__{$columnDefinitionObject->getColumnName()}'";
                }
//                $this->usedClasses[] = "use {$entityNamespace}\\{$tableInfo['largeCamelName']};";
            }
            $joinStr = mackLtrimPlus($joinStr,'->');
            $countString = $str.$joinStr.'->where($where)->count()';
            $idsString = "{$str}{$joinStr}->where(\$where)->orderBy('{$this->mainTableColumnDefinitions['tableInfo']['tableName']}.{$mainTablePrimaryKey}','desc')->limit(\$size)->offset((\$page-1)*\$size)->pluck('{$this->mainTableColumnDefinitions['tableInfo']['tableName']}.{$mainTablePrimaryKey}')";
            $dataString = "{$str}{$joinStr}->whereIn('{$this->mainTableColumnDefinitions['tableInfo']['tableName']}.{$mainTablePrimaryKey}',\$ids)->select({$fieldStr})->get()";
        }
        else
        {
            $countString = $this->stubVariables['{%controllerName%}'].'::where($where)->count()';
            $idsString = $this->stubVariables['{%controllerName%}']."::where(\$where)->orderBy('{$mainTablePrimaryKey}','desc')->limit(\$size)->offset((\$page-1)*\$size)->pluck('{$mainTablePrimaryKey}')";
            $dataString = "{$this->stubVariables['{%controllerName%}']}::whereIn('{$mainTablePrimaryKey}',\$ids)->get()";
        }
        return [$countString,$idsString,$dataString];
    }

    private function getEntityNamespace()
    {
        return str_replace('/','\\',ucfirst(mackLtrimPlus($this->destinationFilePath['destinationDirs']['modelDir'],'@')));
    }

    private function getFields()
    {
        $ignoreFields = [$this->createTimeField,$this->updateTimeField];
        $result = '';
        /** @var CrudColumnDefinitionObjectInterface $columnDefinitionObject */
        foreach ($this->mainTableColumnDefinitions['columnDefinitionObjects'] as $columnDefinitionObject)
        {
            $columnName = $columnDefinitionObject->getColumnName();
            if (!in_array($columnName,$ignoreFields) && !$columnDefinitionObject->isPrimaryKey())
            {
                $result .= ",\"{$columnName}\"";
            }
        }
        return mackLtrimPlus($result,',');
    }

    private function getUsedClasses()
    {
        $this->usedClasses = array_unique($this->usedClasses);
        $str = '';
        foreach ($this->usedClasses as $usedClass)
        {
            $str .= trim($usedClass).PHP_EOL;
        }
        return $str;
    }
}
