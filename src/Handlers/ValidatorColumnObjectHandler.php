<?php


namespace Swoft\Crud\Handlers;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Contract\CrudTableDefinitionObjectInterface;
use Swoft\Crud\Contract\MysqlTableColumnObjectHandlerInterface;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class ValidatorColumnObjectHandler
 * @Bean(name="validatorHandler")
 */
class ValidatorColumnObjectHandler implements MysqlTableColumnObjectHandlerInterface
{

    /**
     * @var array $usedClasses
     */
    private $usedClasses = [];

    /**
     * @var array $stubVariables
     */
    private $stubVariables = [];

    /**
     * @var CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     */
    private $crudTableDefinitionObject;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     */
    private $mainTableColumnDefinitions;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     */
    private $foreignTableColumnDefinitions;

    /**
     * @var array $destinationFilePath
     */
    private $destinationFilePath;

    /**
     * @param CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     * @param CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     * @param CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     * @param array $destinationFilePath
     * @return array
     * @example
     *  [
     *      'filePath'  =>  'fileContent'
     *  ]
     */
    public function handle(CrudTableDefinitionObjectInterface $crudTableDefinitionObject, array $mainTableColumnDefinitions, array $foreignTableColumnDefinitions, array $destinationFilePath): array
    {
        $this->crudTableDefinitionObject = $crudTableDefinitionObject;
        $this->mainTableColumnDefinitions = $mainTableColumnDefinitions;
        $this->foreignTableColumnDefinitions = $foreignTableColumnDefinitions;
        $this->destinationFilePath = $destinationFilePath;

        $result = [];
        foreach ($destinationFilePath['destinationFiles']['validatorFiles'] as $key => $validatorFile)
        {
            if ($key === 'notRequiredValidator')
            {
                $result[$validatorFile] = $this->createValidatorFilesContent(false);
            }
            else
            {
                $result[$validatorFile] = $this->createValidatorFilesContent(true);
            }
        }

        return $result;
    }

    private function createValidatorFilesContent(bool $require)
    {
        $this->initVariables($require);
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/validator.stub'),$this->stubVariables);
    }

    private function initVariables(bool $require)
    {
        $this->stubVariables['{%createDate%}'] = date('Y-m-d H:i:s');
        $this->stubVariables['{%namespace%}'] = $this->getNamespace();
        $this->stubVariables['{%validatorName%}'] = $this->mainTableColumnDefinitions['tableInfo']['largeCamelName'];
        $this->stubVariables['{%validatorName%}'] = $require ? $this->stubVariables['{%validatorName%}'] : 'NotRequired'.$this->stubVariables['{%validatorName%}'];
        $this->stubVariables['{%validatorFields%}'] = $this->getValidatorFields($require);

        //最后再执行这一步
        $this->stubVariables['{%usedClasses%}'] = $this->getUsedClasses($require);
    }

    private function getNamespace()
    {
        return str_replace('/','\\',ucfirst(mackLtrimPlus(dirname($this->destinationFilePath['destinationFiles']['validatorFiles']['validator']),'@')));
    }

    private function getValidatorFields(bool $require)
    {
        $str = '';
        /** @var CrudColumnDefinitionObjectInterface $columnDefinitionObject */
        foreach ($this->mainTableColumnDefinitions['columnDefinitionObjects'] as $columnDefinitionObject)
        {
            // 属性注释头
            $temp = $this->getStubContent('@crud/Stubs/property-comment-header.stub');
            // 属性备注
            $temp .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>$columnDefinitionObject->getLabel()]);
            // 验证注解
            $temp .= $this->getCommentLine($columnDefinitionObject, $require);
            // 属性注释尾
            $temp .= $this->getStubContent('@crud/Stubs/property-comment-footer.stub');
            $temp = rtrim($temp);
            // 属性
            $str .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-full.stub'),['{%propertyComment%}'=>$temp,'{%property%}'=>"protected \${$columnDefinitionObject->getColumnName()};"]);
        }

        return $str;
    }

    private function getCommentLine(CrudColumnDefinitionObjectInterface $columnDefinitionObject, bool $require = true):string
    {
        $string = '';
        $nullString = '|null';

        if ($require)
        {
            $this->usedClasses['Required'] = 'use Swoft\\Validator\\Annotation\\Mapping\\Required;';
            $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@Required()']);

        }

        if (!$columnDefinitionObject->getIsNullable())
        {
            $nullString = '';
        }

        switch ($columnDefinitionObject->getDataType())
        {
            case 'bigint':
            case 'int':
            case 'mediumint':
            case 'smallint':
            case 'tinyint':
                $this->usedClasses['IsInt'] = 'use Swoft\\Validator\\Annotation\\Mapping\\IsInt;';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@IsInt(message=\"{$columnDefinitionObject->getLabel()}必须是一个整数！\")"]);
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@var int']);
                break;
            case 'enum':
                $this->usedClasses['IsString'] = 'use Swoft\\Validator\\Annotation\\Mapping\\IsString;';
                $this->usedClasses['Enum'] = 'use Swoft\\Validator\\Annotation\\Mapping\\Enum;';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@IsString()"]);
                $value = str_replace('\'','"',rtrim(ltrim($columnDefinitionObject->getColumnType(),'enum('),')'));
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@Enum(values={{$value}})"]);
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@var string'.$nullString]);
                break;
            case 'decimal':
            case 'double':
            case 'float':
                $this->usedClasses['IsFloat'] = 'use Swoft\\Validator\\Annotation\\Mapping\\IsFloat;';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@IsFloat()"]);
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@var float']);
                break;
            case 'longtext':
            case 'varchar':
            case 'char':
            case 'text':
            case 'mediumtext':
            case 'smalltext':
            case 'tinytext':
                if (in_array(strtolower($columnDefinitionObject->getColumnName()),['mobile','phone','tel','telephone']))
                {
                    $this->usedClasses['Mobile'] = 'use Swoft\\Validator\\Annotation\\Mapping\\Mobile;';
                    $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@Mobile()"]);
                }
                $this->usedClasses['IsString'] = 'use Swoft\\Validator\\Annotation\\Mapping\\IsString;';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@IsString()"]);
                $this->usedClasses['Length'] = 'use Swoft\\Validator\\Annotation\\Mapping\\Length;';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>"@Length(min=1,max={$columnDefinitionObject->getCharacterMaximumLength()})"]);
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@var string'.$nullString]);
                break;
            case 'year';
            case 'date';
            case 'time';
            case 'datetime';
            case 'timestamp';
                $string .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/property-comment-line.stub'),['{%lineComment%}'=>'@var string'.$nullString]);
                break;
        }

        return $string;
    }

    private function replaceContentVariables(string $content, array $stubVariables): string
    {
        foreach ($stubVariables as $variable => $value)
        {
            $content = str_replace($variable,$value,$content);
        }
        return $content;
    }

    private function getStubContent(string $stub):string
    {
        return file_get_contents(\Swoft::getAlias($stub));
    }

    private function getUsedClasses(bool $require)
    {
        if (!$require)
        {
            mackSafeUnset($this->usedClasses,'Required');
        }

        $str = '';
        foreach ($this->usedClasses as $usedClass)
        {
            $str .= trim($usedClass).PHP_EOL;
        }

        return $str;
    }
}
