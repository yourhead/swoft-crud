<?php


namespace Swoft\Crud\Contract;


interface MysqlTableStructInterface
{
    /**
     * 创建最终生成的Vue文件
     * @return null
     */
    public function run();
}
