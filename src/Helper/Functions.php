<?php declare(strict_types=1);


function mackApiSuccess(Array $data=[], String $msg='请求成功！', Int $code=90000): Array
{
    return ['code'=>$code,'data'=>(object)$data,'msg'=>$msg];
}

function mackApiError(Int $code=90001, String $msg='请求失败！', Array $data=[])
{
    return ['code'=>$code,'data'=>(object)$data,'msg'=>$msg];
}

function mackGetParsedQueryAndBody(\Swoft\Http\Message\Request $request)
{
    return array_merge($request->getParsedQuery(),$request->getParsedBody());
}

if (!function_exists('mb_ucfirst')) {

    function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }

}

function mackToCamelCase($str)
{
    $array = explode('_', $str);
    $result = $array[0];
    $len=count($array);
    if($len>1)
    {
     for($i=1;$i<$len;$i++)
     {
         $result.= ucfirst($array[$i]);
     }
    }
    return $result;
}

function mackParseArrayKeyToCamelCase(array $arr, bool $isLarge=false): array
{
    $result = array();
    foreach ($arr as $k => $item)
    {
        $resultKey = mackToCamelCase(strtolower($k));
        if ($isLarge)
        {
            $resultKey = ucfirst($resultKey);
        }
        $result[$resultKey] = $item;
    }
    return $result;
}

function mackParseYesOrNoToBoolean(string $yesOrNo) : bool
{
    return strtolower($yesOrNo) == 'yes';
}

function mackLtrimPlus($str,$pre):string
{
    $position = strpos($str,$pre);
    if ($position === 0)
    {
        return mb_substr($str,mb_strlen($pre));
    }
    else
    {
        return $str;
    }
}

function mackCreatePassword($password,$salt):string
{
    return md5(md5($password).$salt);
}


function mackMkdirForFile(string $fullFileName)
{
    $dirName = dirname($fullFileName);
    if (!is_dir($dirName))
    {
        mkdir($dirName,755, true);
    }
}

function mackSafeUnset(array &$arr,string $key)
{
    if (isset($arr[$key]))
    {
        unset($arr[$key]);
    }
}
