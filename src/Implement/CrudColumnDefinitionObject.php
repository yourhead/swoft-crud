<?php


namespace Swoft\Crud\Implement;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Exceptions\KeyNotExistsException;
use Swoft\Crud\Exceptions\MethodNotExistsException;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class CrudColumnDefinitionObject
 * @Bean(name="crudDefinition",scope="prototype")
 * @method string getColumnName()
 * @method string getColumnKey()
 * @method string getDataType()
 * @method bool getIsNullable()
 * @method string getColumnType()
 * @method int getCharacterMaximumLength()
 * @method mixed getColumnDefault()
 */
class CrudColumnDefinitionObject implements CrudColumnDefinitionObjectInterface
{
    /**
     * @var MysqlTableColumnStruct $columnStruct
     */
    private $columnStruct;

    /**
     * @var MysqlColumnCommentObject $commentObject
     */
    private $commentObject;

    /**
     * @var array $reslult
     */
    private $reslult = [];

    /**
     * @var array $viewProperties
     */
    private $viewProperties;

    /**
     * @var array $formItemProperties
     */
    private $formItemProperties;

    /**
     * @var string $searchType
     */
    private $searchType = '=';

    /**
     * @return MysqlTableColumnStruct
     */
    public function getColumnStruct(): MysqlTableColumnStruct
    {
        return $this->columnStruct;
    }

    /**
     * @param MysqlTableColumnStruct $columnStruct
     */
    public function setColumnStruct(MysqlTableColumnStruct $columnStruct): void
    {
        $this->columnStruct = $columnStruct;
    }

    public function initByColumnStruct(MysqlTableColumnStruct $columnStruct)
    {
        $this->setColumnStruct($columnStruct);
        $this->commentObject = \bean('columnCommentObject');
        $this->commentObject->initByCommentString($columnStruct->getColumnComment());
//        $this->mergeDefinitionComment();
    }

    public function __call($name, $arguments)
    {
        if (strpos($name,'get') === 0)
        {
            try
            {
                return $this->commentObject->$name();
            }
            catch (KeyNotExistsException $exception)
            {
                return $this->columnStruct->$name();
            }
        }
        else
        {
            throw new MethodNotExistsException("Method {$name} not exists!");
        }
    }

    /**
     * @return bool
     */
    public function isPrimaryKey(): bool
    {
        return $this->getColumnKey() === 'PRI';
    }

    public function getLabel(): string
    {
        if (!isset($this->reslult['label']))
        {
            try
            {
                $this->reslult['label'] = $this->commentObject->getLabel();
            }
            catch (KeyNotExistsException $exception)
            {
                $this->reslult['label'] = '未命名';
            }
        }

        return $this->reslult['label'];
    }

    public function isShow(): bool
    {
        if (!isset($this->reslult['isShow']))
        {
            try
            {
                $this->reslult['isShow'] = $this->commentObject->getIsShow();
            }
            catch (KeyNotExistsException $exception)
            {
                $this->reslult['isShow'] = true;
            }
        }

        return $this->reslult['isShow'];
    }

    public function getViewProperties(): string
    {
        $str = '';
        $viewProperties = $this->getViewPropertiesArray();
        foreach ($viewProperties as $key => $viewProperty)
        {
            $str .= "{$key}=\"{$viewProperty}\" ";
        }
        return $str;
    }

    public function getFormItemProperties(): string
    {
        $str = '';
        $itemProperties = $this->getFormItemPropertiesArray();
        foreach ($itemProperties as $key => $itemProperty)
        {
            $str .= "{$key}=\"{$itemProperty}\" ";
        }
        return $str;
    }

    private function getViewPropertiesArray()
    {
        if ($this->viewProperties === null)
        {
            $this->viewProperties = [
                'label'=>$this->getLabel(),
            ];
            $this->initViewPropertyFromComment();
        }

        return $this->viewProperties;
    }

    private function getFormItemPropertiesArray()
    {
        if ($this->formItemProperties === null)
        {
            $this->formItemProperties = [
                'label'=>$this->getLabel(),
            ];
            $this->initFormItemPropertyFromComment();
        }

        return $this->formItemProperties;
    }

    private function setViewPropertyFromComment(string $propertyName)
    {
        $result = '';

        try
        {
            $method = 'get'.ucfirst($propertyName);
            $this->reslult[$propertyName] = $this->commentObject->$method();
        }
        catch (KeyNotExistsException $exception)
        {

        }

        if ($result)
        {
            $this->viewProperties[$propertyName] = $result;
        }
    }

    private function initViewPropertyFromComment()
    {
        try
        {
            $properties = $this->commentObject->getViewProperties();
            if (is_array($properties))
            {
                foreach ($properties as $key => $property)
                {
                    $this->viewProperties[$key] = $property;
                }
            }
        }
        catch (KeyNotExistsException $exception)
        {

        }
    }

    private function initFormItemPropertyFromComment()
    {
        try
        {
            $properties = $this->commentObject->getFormItemProperties();
            if (is_array($properties))
            {
                foreach ($properties as $key => $property)
                {
                    $this->formItemProperties[$key] = $property;
                }
            }
        }
        catch (KeyNotExistsException $exception)
        {

        }
    }

    public function getViewType(): string
    {
        try
        {
            return $this->commentObject->getViewType();
        }
        catch (KeyNotExistsException $exception)
        {
            $dataType = $this->getDataType();
            switch ($dataType)
            {
                case 'bigint':
                case 'int':
                case 'mediumint':
                case 'smallint':
                case 'tinyint':
                    return 'intNumber';
                case 'decimal':
                case 'double':
                case 'float':
                    return 'floatNumber';
                case 'longtext':
                case 'text':
                case 'mediumtext':
                case 'smalltext':
                case 'tinytext':
                    return 'longString';
                case 'varchar':
                case 'char':
                    return 'shortString';
                case 'year';
                case 'date';
                    return 'date';
                case 'time';
                case 'datetime';
                case 'timestamp';
                    return 'dateTime';
                case 'enum':
                    return 'enum';
                default:
                    return 'shortString';
            }
        }
    }

    public function getViewOptions(): array
    {
        $options = [];
        try
        {
            $options = $this->commentObject->getViewOptions();
        }
        catch (KeyNotExistsException $exception)
        {

        }
        foreach ($options as $k => $option)
        {
            if (!isset($option['style']))
            {
                $options[$k]['style'] = 'success';
            }
            if (!isset($option['text']))
            {
                $options[$k]['text'] = '请设置viewOptions';
            }
        }
        return $options;
    }

    public function canSearch(): bool
    {
        try
        {
            $searchType = $this->commentObject->getSearchType();
            $searchType = $searchType ? : '=';
            $this->searchType = $searchType;
            return true;
        }
        catch (KeyNotExistsException $exception)
        {
            return false;
        }
    }

    public function getSearchType(): string
    {
        return $this->searchType;
    }
}
