<?php


namespace Swoft\Crud\Handlers;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Contract\CrudTableDefinitionObjectInterface;
use Swoft\Crud\Contract\MysqlTableColumnObjectHandlerInterface;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class ViewColumnObjectHandler
 * @Bean(name="viewHandler")
 */
class ViewColumnObjectHandler implements MysqlTableColumnObjectHandlerInterface
{
    /**
     * @var array $stubVariables
     */
    private $stubVariables = [];

    /**
     * @var CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     */
    private $crudTableDefinitionObject;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     */
    private $mainTableColumnDefinitions;

    /**
     * @var CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     */
    private $foreignTableColumnDefinitions;

    /**
     * @var array $destinationFilePath
     */
    private $destinationFilePath;

    /**
     * @param CrudTableDefinitionObjectInterface $crudTableDefinitionObject
     * @param CrudColumnDefinitionObjectInterface [] $mainTableColumnDefinitions
     * @param CrudColumnDefinitionObjectInterface [] $foreignTableColumnDefinitions
     * @param array $destinationFilePath
     * @return array
     * @example
     *  [
     *      'filePath'  =>  'fileContent'
     *  ]
     */
    public function handle(CrudTableDefinitionObjectInterface $crudTableDefinitionObject, array $mainTableColumnDefinitions, array $foreignTableColumnDefinitions, array $destinationFilePath): array
    {
        $this->crudTableDefinitionObject = $crudTableDefinitionObject;
        $this->mainTableColumnDefinitions = $mainTableColumnDefinitions;
        $this->foreignTableColumnDefinitions = $foreignTableColumnDefinitions;
        $this->destinationFilePath = $destinationFilePath;

        $result = [];
        foreach ($destinationFilePath['destinationFiles']['viewFiles'] as $key => $viewFile)
        {
            if (strpos($viewFile,'.vue') != false)
            {
                $result[$viewFile] = $this->createVueFileContent();
            }

            if ($key == 'api')
            {
                $result[$viewFile] = $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/api.stub'),$this->stubVariables);
            }

            if ($key == 'router')
            {
                $result[$viewFile] = $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/router.stub'),$this->stubVariables);
            }
        }

        return $result;
    }

    private function createVueFileContent()
    {
        $this->initVueFileVariables();
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/vue.stub'),$this->stubVariables);
    }

    private function replaceContentVariables(string $content, array $stubVariables): string
    {
        foreach ($stubVariables as $variable => $value)
        {
            $content = str_replace($variable,$value,$content);
        }
        return $content;
    }

    private function getStubContent(string $stub):string
    {
        return file_get_contents(\Swoft::getAlias($stub));
    }

    private function initVueFileVariables()
    {
        $this->stubVariables['{%templateLine%}'] = $this->getTemplateLineContent();
        $this->stubVariables['{%scriptLine%}'] = $this->getScriptLineContent();
        $this->stubVariables['{%tableLargeCamelName%}'] = $this->mainTableColumnDefinitions['tableInfo']['largeCamelName'];
        $this->stubVariables['{%tableSmallCamelName%}'] = $this->mainTableColumnDefinitions['tableInfo']['smallCamelName'];
        $this->stubVariables['{%primaryKeyLabel%}'] = $this->crudTableDefinitionObject->getPrimaryKey()->getLabel();
        $this->stubVariables['{%primaryKeyName%}'] = $this->crudTableDefinitionObject->getPrimaryKey()->getColumnName();
    }

    private function getTemplateLineContent()
    {
        $this->stubVariables['{%filterContainerLine%}'] = $this->getFilterContainerLineContent();
        $this->stubVariables['{%tableLine%}'] = $this->getTableLine();
        $this->stubVariables['{%paginationLine%}'] = $this->getPaginationLine();
        $this->stubVariables['{%formLine%}'] = $this->getFormLine();

        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/template.stub'),$this->stubVariables);
    }

    private function getFormLine()
    {
        $this->stubVariables['{%formItemLines%}'] = $this->getFormItemLines();
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/form.stub'),$this->stubVariables);
    }

    private function getFormItemLines()
    {
        $content = '';
        foreach ($this->mainTableColumnDefinitions['columnDefinitionObjects'] as $mainTableColumnDefinition)
        {
            $content .= $this->getFormItemLine($mainTableColumnDefinition);
        }
        return $content;
    }

    private function getFormItemLine(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $content = '';
        if (!$columnDefinitionObject->isPrimaryKey())
        {
            $this->stubVariables['{%formItemProperties%}'] = $columnDefinitionObject->getFormItemProperties();
            $this->stubVariables['{%itemLine%}'] = $this->getItemLine($columnDefinitionObject);
            $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/form-item.stub'),$this->stubVariables);
        }
        return $content;
    }

    private function getItemLine(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $variables = [
            '{%columnName%}'=>'temp.'.mackToCamelCase($columnDefinitionObject->getColumnName()),
            '{%placeholder%}'=>$columnDefinitionObject->getLabel(),
            '{%extra%}'=>'',
            '{%columnOptions%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()).'Options'
        ];
        switch ($columnDefinitionObject->getViewType())
        {
            case 'intNumber':
            case 'floatNumber':
                $variables['{%placeholder%}'] = '请输入'.$variables['{%placeholder%}'];
                $variables['{%type%}'] = 'number';
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-input.stub')),$variables);
            default:
            case 'shortString':
                $variables['{%placeholder%}'] = '请输入'.$variables['{%placeholder%}'];
                $variables['{%type%}'] = 'text';
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-input.stub')),$variables);
            case 'longString':
                $variables['{%placeholder%}'] = '请输入'.$variables['{%placeholder%}'];
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-textarea.stub')),$variables);
            case 'date':
                $variables['{%type%}'] = 'date';
                $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-datetime.stub')),$variables);
            case 'dateTime':
                $variables['{%type%}'] = 'datetime';
                $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-datetime.stub')),$variables);
            case 'enum':
                $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                return $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/el-select.stub')),$variables);
        }
    }

    private function getPaginationLine()
    {
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/pagination.stub'),$this->stubVariables);
    }

    private function getFilterContainerLineContent()
    {
        // set variables

        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/filter-container.stub'),$this->stubVariables);
    }

    private function getTableLine()
    {
        $this->stubVariables['{%tableColumns%}'] = $this->getTableColumns();
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/table.stub'),$this->stubVariables);
    }

    private function getTableColumns()
    {
        $content = '';
        // 主表列
        foreach ($this->mainTableColumnDefinitions['columnDefinitionObjects'] as $columnDefinitionObject)
        {
            $content .= $this->getTableColumn($columnDefinitionObject);
        }

        // 关联表列
        foreach ($this->foreignTableColumnDefinitions['columnDefinitionObjects'] as $foreignTableName => $fColumnDefinitionObjects) {
            foreach ($fColumnDefinitionObjects as $fColumnDefinitionObject)
            {
                $content .= $this->getTableColumn($fColumnDefinitionObject,$foreignTableName.'__');
            }
        }

        return $content;
    }

    private function getTableColumn(CrudColumnDefinitionObjectInterface $columnDefinitionObject, string $prefix='')
    {
        $content = '';
        if ($columnDefinitionObject->isShow())
        {
            $this->stubVariables['{%elTableColumnProperties%}'] = $columnDefinitionObject->getViewProperties();
            $this->stubVariables['{%slotScope%}'] = $this->getSlotScope($columnDefinitionObject,$prefix);
            $this->stubVariables['{%columnLines%}'] = $this->getColumnLines($columnDefinitionObject,$prefix);
            $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/table-column-common.stub'),$this->stubVariables);
        }
        return $content;
    }

    private function getSlotScope(CrudColumnDefinitionObjectInterface $columnDefinitionObject, string $prefix='')
    {
        return '{row,$index}';
    }

    private function getColumnLines(CrudColumnDefinitionObjectInterface $columnDefinitionObject, string $prefix='')
    {
        switch ($columnDefinitionObject->getViewType())
        {
            default:
            case 'intNumber':
            case 'floatNumber':
            case 'shortString':
                $this->stubVariables['{%columnData%}'] = 'row.'.$prefix.$columnDefinitionObject->getColumnName();
                break;
            case 'longString':
                $this->stubVariables['{%columnData%}'] = '点击查看详情';
                break;
            case 'date':
                $this->stubVariables['{%columnData%}'] = 'row.'.$prefix.$columnDefinitionObject->getColumnName().' | parseTime(\'{y}-{m}-{d}\')';
                break;
            case 'dateTime':
                $this->stubVariables['{%columnData%}'] = 'row.'.$prefix.$columnDefinitionObject->getColumnName().' | parseTime(\'{y}-{m}-{d} {h}:{i}\')';
                break;
            case 'enum':
                $options = $columnDefinitionObject->getViewOptions();
                $text = '';
                foreach ($options as $status => $option)
                {
                    $variables = [
                        '{%tagCondition%}'=>"row.{$prefix}{$columnDefinitionObject->getColumnName()} == '{$status}'",
                        '{%tagStyle%}'=>$option['style'],
                        '{%tagText%}'=>$option['text']
                    ];
                    $text .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-tag.stub'),$variables);
                }
                return $text;
        }
        return  $this->replaceContentVariables(rtrim($this->getStubContent('@crud/Stubs/views/common/table-span.stub')),$this->stubVariables);
    }

    private function getScriptLineContent()
    {
        $this->stubVariables['{%scriptOptions%}'] = $this->getScriptMainTableEach('getColumnScriptOption');
        $this->stubVariables['{%optionsKeyValue%}'] = $this->getScriptMainTableEach('getColumnScriptOptionKV');
        $this->stubVariables['{%typeStyleMap%}'] = $this->getScriptMainTableEach('getColumnScriptTypeStyleMap');
        $this->stubVariables['{%typeFilters%}'] = rtrim($this->getScriptMainTableEach('getColumnScriptOptionTypeFilter'),',');
        $this->stubVariables['{%tempObject%}'] = rtrim($this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-temp-object.stub'),['{%tempLines%}'=>rtrim(rtrim($this->getScriptMainTableEach('getTempObject')),',')]));
        $this->stubVariables['{%ruleLines%}'] = rtrim($this->getScriptMainTableEach('getRuleLine'),',');
        $this->stubVariables['{%definedOptions%}'] = rtrim($this->stubVariables['{%definedOptions%}']);
        $this->getScriptMainTableEach('setSearchVariables');
        $this->stubVariables['{%searchLines%}'] = rtrim($this->stubVariables['{%searchLines%}']);
        $this->stubVariables['{%tableFilterLines%}'] = rtrim($this->stubVariables['{%tableFilterLines%}']);
        $this->getScriptMainTableEach('setColumnKeyString');
        $this->stubVariables['{%columnKeyString%}'] = mackLtrimPlus($this->stubVariables['{%columnKeyString%}'],',');

        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/script.stub'),$this->stubVariables);
    }

    private function getScriptMainTableEach(string $functionName)
    {
        $content = '';

        foreach ($this->mainTableColumnDefinitions['columnDefinitionObjects'] as $columnDefinitionObject)
        {
            $content .= $this->$functionName($columnDefinitionObject);
        }

        $content = rtrim($content);

        return $content;
    }

    private function setColumnKeyString(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        if (!isset($this->stubVariables['{%columnKeyString%}']))
        {
            $this->stubVariables['{%columnKeyString%}'] = '';
        }

        $this->stubVariables['{%columnKeyString%}'] .= ",'{$columnDefinitionObject->getColumnName()}'";
    }

    private function setSearchVariables(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        if (!isset($this->stubVariables['{%searchLines%}']))
        {
            $this->stubVariables['{%searchLines%}'] = '';
            $this->stubVariables['{%tableFilterLines%}'] = '';
        }

        if ($columnDefinitionObject->canSearch())
        {
            $this->stubVariables['{%searchLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-search-line.stub'),['{%key%}'=>$columnDefinitionObject->getColumnName()]);

            $variables = [
                '{%columnName%}'=>'listQuery.'.mackToCamelCase($columnDefinitionObject->getColumnName()),
                '{%placeholder%}'=>$columnDefinitionObject->getLabel(),
                '{%extra%}'=>'',
                '{%columnOptions%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()).'Options'
            ];
            switch ($columnDefinitionObject->getViewType())
            {
                case 'intNumber':
                case 'floatNumber':
                    $variables['{%placeholder%}'] = '请输入'.$variables['{%placeholder%}'];
                    $variables['{%type%}'] = 'number';
                    $variables['{%extra%}'] = ' style="width: 200px;" class="filter-item" @keyup.enter.native="handleFilter" ';
                    $this->stubVariables['{%tableFilterLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-input.stub'),$variables);
                    break;
                default:
                case 'longString':
                case 'shortString':
                    $variables['{%placeholder%}'] = '请输入'.$variables['{%placeholder%}'];
                    $variables['{%type%}'] = 'text';
                    $variables['{%extra%}'] = ' style="width: 200px;" class="filter-item" @keyup.enter.native="handleFilter" ';
                    $this->stubVariables['{%tableFilterLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-input.stub'),$variables);
                    break;
                case 'date':
                    $variables['{%type%}'] = 'date';
                    $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                    $variables['{%extra%}'] = ' style="width: 200px;" class="filter-item" @keyup.enter.native="handleFilter" ';
                    $this->stubVariables['{%tableFilterLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-datetime.stub'),$variables);
                    break;
                case 'dateTime':
                    $variables['{%type%}'] = 'datetime';
                    $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                    $variables['{%extra%}'] = ' style="width: 200px;" class="filter-item" @keyup.enter.native="handleFilter" ';
                    $this->stubVariables['{%tableFilterLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-datetime.stub'),$variables);
                    break;
                case 'enum':
                    $variables['{%placeholder%}'] = '请选择'.$variables['{%placeholder%}'];
                    $variables['{%extra%}'] = 'clearable class="filter-item" style="width: 130px" ';
                    $this->stubVariables['{%tableFilterLines%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/el-select.stub'),$variables);
                    break;
            }
        }
    }

    private function getTempObject(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $variables = ['{%field%}'=>$columnDefinitionObject->getColumnName(),'{%fieldValue%}'=>'undefined'];
        switch ($columnDefinitionObject->getViewType())
        {
            case 'intNumber':
            case 'floatNumber':
                $variables['{%fieldValue%}'] = $columnDefinitionObject->getColumnDefault();
                $variables['{%fieldValue%}'] = $variables['{%fieldValue%}'] === null ? 0 : $variables['{%fieldValue%}'];
                break;
            case 'date':
            case 'dateTime':
                $variables['{%fieldValue%}'] = 'new Date()';
                break;
            default:
                $variables['{%fieldValue%}'] = "'{$columnDefinitionObject->getColumnDefault()}'";
                break;
        }
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-temp-line.stub'),$variables);
    }

    private function getRuleLine(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $variables = [
            '{%key%}'=>$columnDefinitionObject->getColumnName(),
            '{%label%}'=>$columnDefinitionObject->getLabel(),
            '{%trigger%}'=>'blur',
            '{%extra%}'=>''
        ];
        switch ($columnDefinitionObject->getViewType())
        {
            case 'date':
            case 'dateTime':
                $variables['{%trigger%}'] = 'change';
                $variables['{%extra%}'] = ',type: "date" ';
                break;
            default:
                $variables['{%fieldValue%}'] = "'{$columnDefinitionObject->getColumnDefault()}'";
                break;
        }
        return $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-rule-line.stub'),$variables);
    }

    private function getColumnScriptOption(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $content = '';
        switch ($columnDefinitionObject->getViewType())
        {
            case 'enum':
                $options = $columnDefinitionObject->getViewOptions();
                $variables = [
                    '{%optionName%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()),
                    '{%optionLines%}'=>''
                ];
                $lines = '';
                foreach ($options as $status => $option)
                {
                    $lineVariables = [
                        '{%optionKey%}'=>is_string($status) ? "'{$status}'" : $status,
                        '{%optionDisplayName%}'=>"'{$option['text']}'"
                    ];
                    $lines .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-option-line.stub'),$lineVariables);
                }
                $variables['{%optionLines%}'] = rtrim(rtrim($lines),',');
                $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-options.stub'),$variables);
                if (!isset($this->stubVariables['{%definedOptions%}']))
                {
                    $this->stubVariables['{%definedOptions%}'] = '';
                }
                $this->stubVariables['{%definedOptions%}'] .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-defined-options.stub'),$variables);
                break;
        }
        return $content;
    }

    private function getColumnScriptTypeStyleMap(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $content = '';
        switch ($columnDefinitionObject->getViewType())
        {
            case 'enum':
                $options = $columnDefinitionObject->getViewOptions();
                $variables = [
                    '{%optionName%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()),
                    '{%mapLines%}'=>''
                ];
                $lines = '';
                foreach ($options as $status => $option)
                {
                    $lineVariables = [
                        '{%key%}'=>$status,
                        '{%style%}'=>$option['style']
                    ];
                    $lines .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-map-line.stub'),$lineVariables);
                }
                $variables['{%mapLines%}'] = rtrim(rtrim($lines),',');
                $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-type-style-map.stub'),$variables);
                break;
        }
        return $content;
    }

    private function getColumnScriptOptionKV(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $content = '';
        switch ($columnDefinitionObject->getViewType())
        {
            case 'enum':
                $variables = [
                    '{%optionName%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()),
                ];
                $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-options-kv.stub'),$variables);
                break;
        }
        return $content;
    }

    private function getColumnScriptOptionTypeFilter(CrudColumnDefinitionObjectInterface $columnDefinitionObject)
    {
        $content = '';
        switch ($columnDefinitionObject->getViewType())
        {
            case 'enum':
                $variables = [
                    '{%optionName%}'=>mackToCamelCase($columnDefinitionObject->getColumnName()),
                ];
                $content .= $this->replaceContentVariables($this->getStubContent('@crud/Stubs/views/common/script-type-filters.stub'),$variables);
                break;
        }
        return $content;
    }


}
