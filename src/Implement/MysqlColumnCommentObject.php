<?php


namespace Swoft\Crud\Implement;


use Swoft\Crud\Contract\MysqlColumnCommentObjectInterface;
use Swoft\Crud\Exceptions\KeyNotExistsException;
use Swoft\Crud\Exceptions\MethodNotExistsException;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class MysqlColumnCommentObject
 * @Bean(name="columnCommentObject",scope="prototype")
 * @method string getLabel()
 */
class MysqlColumnCommentObject implements MysqlColumnCommentObjectInterface
{
    /**
     * @var string $comment
     */
    private $comment;

    /**
     * @var array $commentJsonArray
     */
    private $commentJsonArray;

    /**
     * @var array $result
     */
    private $result = [];

    public function initByCommentString(string $comment)
    {
        $this->comment = $comment;
        $this->commentJsonArray = json_decode($comment,true);
        // 如果注释不是一个有效的json，则将注释的内容作为label字段使用
        if (!$this->commentJsonArray && $comment)
        {
            $this->result['label'] = $comment;
        }
    }

    public function __call($name, $arguments)
    {
        if (strpos($name,'get') === 0)
        {
            $key = lcfirst(mackLtrimPlus($name,'get'));
            if (isset($this->result[$key]))
            {
                return $this->result[$key];
            }
            if ($this->commentJsonArray && isset($this->commentJsonArray[$key]))
            {
                $this->result[$key] = $this->commentJsonArray[$key];
                return $this->result[$key];
            }
            throw new KeyNotExistsException("Key {$key} not exists!");
        }
        else
        {
            throw new MethodNotExistsException("Method {$name} not exists!");
        }
    }
}
