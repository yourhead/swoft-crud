<?php


namespace Swoft\Crud;

use Swoft\Crud\Exceptions\CrudException;
use Swoft\Crud\Implement\MysqlTableStruct;
use Swoft\Console\Annotation\Mapping\Command;
use Swoft\Console\Annotation\Mapping\CommandMapping;
use Swoft\Console\Annotation\Mapping\CommandOption;
use Swoft\Console\Input\Input;
use Swoft\Console\Output\Output;

/**
 * Class Crud
 * @Command(name="crud",alias="curd",coroutine=true,desc="Generate CRUD operation which based on vue-element-admin")
 * @example
 *  {groupName}:crud Generate CRUD operation
 */
class Crud
{
    /**
     * @var Input $input
     */
    private $input;

    /**
     * @var Output $output
     */
    private $output;

    /**
     * @var MysqlTableStruct $tableStruct
     */
    private $tableStruct;

    /**
     * @CommandMapping(name="create",alias="gen",desc="Generate CRUD operation",usage="{fullCommand} [-t|--table]")
     * @CommandOption("table",short="t",type="string",desc="the destination table name")
     * @CommandOption("relation",short="r",type="array",desc="the relation table names")
     * @CommandOption("relationKey",short="rk",type="array",desc="the relation keys")
     * @CommandOption("relationPrimaryKey",short="rpk",type="array",desc="the relation primary keys")
     * @CommandOption("force",short="f",type="boolean",desc="if file is already exists, it will be rewrite")
     * @return null
     */
    public function create()
    {
        $this->prepare();
        try
        {
            $this->tableStruct->run();
        }
        catch (CrudException $throwable)
        {
            $this->output->error('Do crud failed! Message : '.$throwable->getMessage());
        }
    }

    private function prepare()
    {
        $this->input = bean('input');
        $this->output = bean('output');
        $this->tableStruct = bean('tableStruct');
    }
}
