<?php


namespace Swoft\Crud\Implement;


use Swoft\Crud\Contract\MysqlTableColumnObjectHandlerInterface;
use Swoft\Crud\Contract\MysqlTableStructInterface;
use Swoft\Crud\Exceptions\FileAlreadyExistsException;
use Swoft\Crud\Exceptions\ForeignKeyNotMatchException;
use Swoft\Crud\Exceptions\TableNotFoundException;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Console\Input\Input;
use Swoft\Console\Output\Output;
use Swoft\Db\Database;
use Swoft\Db\DB;
use Swoft\Stdlib\Helper\ArrayHelper;

/**
 * Class MysqlTableStruct
 * @Bean(name="tableStruct",scope="prototype")
 */
class MysqlTableStruct implements MysqlTableStructInterface
{
    /**
     * @var array $columns
     */
    private $columns;

    private $relations = [];

    private $definitionObjects = [];

    private $fileInfo = [];

    private $destinationDirs = [
        'controllerDir'    =>  '@app/Http/Admin/Controller',
        'modelDir'         =>  '@app/Model/Common/Entity',
        'validatorDir'        =>  '@app/Validator/Common',
        'viewsDir'            =>  '@base/resource/admin',
    ];

    private $destinationFiles = [];

    /**
     * @var CrudTableDefinitionObject $tableDefinitionObject
     */
    private $tableDefinitionObject;

    /**
     * @var array $originData
     */
    private $originData;

    /**
     * @var array $foreignOriginData
     */
    private $foreignOriginData = [];

    /**
     * @var Input $input
     */
    private $input;

    /**
     * @var Output $output
     */
    private $output;

    /**
     * @var string $tableName
     */
    private $tableName;

    /**
     * @var string $smallCamelName
     */
    private $smallCamelName;

    /**
     * @var string $largeCamelName
     */
    private $largeCamelName;

    /**
     * @var string $dbName
     */
    private $dbName;

    /**
     * @var Database $db;
     */
    private $db;

    /**
     * @var string $tableFullName
     */
    private $tableFullName;

    /**
     * @var array $handlers
     */
    private $handlers;

    /**
     * @example
     *  [
     *      'tableName' =>  [//关联表名
     *          'foreignKey'    =>  'keyName',//外键名
     *          'foreignPrimaryKey' =>  'keyName',//关联表主键
     *      ],
     *  ]
     * @var array $foreignTables
     */
    private $foreignTables = [];

    /**
     * @var array $foreignColums
     */
    private $foreignColums = [];

    /**
     * @var array $foreignDefinitionObjects
     */
    private $foreignDefinitionObjects = [];

    /**
     * 创建最终生成的文件
     * @return null
     */
    public function run()
    {
        $this->prepare();
        $this->initTableAndColumnInfo();
        $this->runHandlers();
        $this->writeFiles();
    }

    private function writeFiles()
    {
        foreach ($this->fileInfo as $filePath => $content)
        {
            $realPath = \Swoft::getAlias($filePath);
            mackMkdirForFile($realPath);
            file_put_contents($realPath,$content);
            $this->output->success("Write file {$realPath} success!");
        }
    }

    private function runHandlers()
    {
        /** @var MysqlTableColumnObjectHandlerInterface $handler */
        foreach ($this->handlers as $handler)
        {
            if ($handler instanceof MysqlTableColumnObjectHandlerInterface)
            {
                $mainTable = [
                    'tableInfo'=>[
                        'tableFullName'=>$this->tableFullName,
                        'tableName'=>$this->tableName,
                        'smallCamelName'=>$this->smallCamelName,
                        'largeCamelName'=>$this->largeCamelName
                    ],
                    'columnDefinitionObjects'=>$this->definitionObjects
                ];
                $foreignTable = [
                    'tableInfos'=>$this->foreignTables,
                    'columnDefinitionObjects'=>$this->foreignDefinitionObjects
                ];
                $pathInfo = [
                    'destinationDirs'=>$this->destinationDirs,
                    'destinationFiles'=>$this->destinationFiles,
                ];
                $fileInfo = $handler->handle($this->tableDefinitionObject,$mainTable,$foreignTable,$pathInfo);
                $this->fileInfo = ArrayHelper::merge($this->fileInfo,$fileInfo);
            }
        }
    }

    /**
     * 准备工作
     */
    private function prepare()
    {
        $this->input = \bean('input');
        $this->output = \bean('output');
        $this->db = bean('db');
        $destinationDirs = config('crud.destinationDirs');
        if ($destinationDirs)
        {
            $this->destinationDirs = ArrayHelper::merge($this->destinationDirs,$destinationDirs);
        }
        $this->setDbName(explode('dbname=',explode(';',$this->db->getDsn())[0])[1]);
        $this->addHandlers();
    }

    /**
     * @param string $tableName
     */
    private function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

    /**
     * @return string
     */
    private function getDbName(): string
    {
        return $this->dbName;
    }

    /**
     * @param string $dbName
     */
    private function setDbName(string $dbName): void
    {
        $this->dbName = $dbName;
    }

    private function addHandlers()
    {
        $this->handlers = [
            \bean('validatorHandler'),
            \bean('modelHandler'),
            \bean('controllerHandler'),
            \bean('viewHandler'),
        ];
    }

    private function initTableAndColumnInfo()
    {
        $this->checkTable();
        $this->checkFileExists();
        $this->setOriginData();
        $this->setTableInfo();
        $this->initColumnStructs();
        $this->initDefinitionObjects();

        $this->tableDefinitionObject->initByColumnDefinition($this->definitionObjects);
    }

    private function initDefinitionObjects()
    {
        $this->definitionObjects = $this->initDefinitionByColumnStructs($this->columns);
        foreach ($this->foreignColums as $tableName => $foreignColum)
        {
            $this->foreignDefinitionObjects[$tableName] = $this->initDefinitionByColumnStructs($foreignColum);
        }
    }

    private function initDefinitionByColumnStructs($columns)
    {
        $temp = [];
        /** @var MysqlTableColumnStruct $column */
        foreach ($columns as $column) {
            /** @var CrudColumnDefinitionObject $definitionObject */
            $definitionObject = \bean('crudDefinition');
            $definitionObject->initByColumnStruct($column);
            $temp[] = $definitionObject;
        }
        return $temp;
    }

    private function parseOriginDataToColumnStructs($originData):array
    {
        $result = array();
        foreach ($originData as $originDatum)
        {
            unset($originDatum['TABLE_CATALOG']);
            unset($originDatum['TABLE_SCHEMA']);
            unset($originDatum['TABLE_NAME']);
            /** @var MysqlTableColumnStruct $columnStruct */
            $columnStruct = \bean('columnStruct');
            $columnStruct->setOriginData($originDatum);
            $columnStruct->initByDefinition();
            $result[] = $columnStruct;
        }
        return $result;
    }

    private function setOriginData()
    {
        $this->originData = $this->getOriginData($this->dbName, $this->tableFullName);
        //检查外键
        $this->checkForeignKeys();
        if ($this->foreignTables)
        {
            foreach ($this->foreignTables as $foreignTableName => $info)
            {
                $this->foreignOriginData[$foreignTableName] = $this->getOriginData($this->dbName, $info['tableFullName']);
                //检查关联表主键
                $this->checkKey($info,$this->foreignOriginData[$foreignTableName],'relationPrimaryKey');
            }
        }
    }

    private function checkForeignKeys()
    {
        foreach ($this->foreignTables as $foreignTable) {
            $this->checkKey($foreignTable,$this->originData,'relationKey');
        }
    }

    /**
     * @param $foreignTable
     * @param $originData
     * @param $checkKeyName
     * @throws ForeignKeyNotMatchException
     */
    private function checkKey($foreignTable, $originData, $checkKeyName)
    {
        if (!in_array($foreignTable[$checkKeyName],array_column($originData,'COLUMN_NAME')))
        {
            throw new ForeignKeyNotMatchException("Can not found key {$foreignTable[$checkKeyName]} from table {$originData[0]['TABLE_NAME']}!");
        }
    }

    private function getOriginData(string $dbName,string $tableFullName)
    {
        //从数据库中获取表字段信息
        $sql = "SELECT * FROM `information_schema`.`columns` "
            . "WHERE TABLE_SCHEMA = ? AND table_name = ? "
            . "ORDER BY ORDINAL_POSITION";
        //加载主表的列
        return Db::select($sql, [$dbName, $tableFullName]);
    }

    private function checkFileExists() : bool
    {
        $this->addDestinationFiles();

        if ($this->input->getOption('force'))
        {
            return true;
        }

        $arrayIterator = new \RecursiveArrayIterator($this->destinationFiles);
        $iterator = new \RecursiveIteratorIterator($arrayIterator);
        foreach ($iterator as $item)
        {
            $file = \Swoft::getAlias($item);
            if (file_exists($file))
            {
                throw new FileAlreadyExistsException('File '.$file. ' already exists!');
            }
        }

        return true;
    }

    private function addDestinationFiles()
    {
        $this->destinationFiles['controllerFile'] = rtrim($this->destinationDirs['controllerDir'],'/')."/{$this->largeCamelName}Controller.php";
        $this->destinationFiles['modelFile'] = rtrim($this->destinationDirs['modelDir'],'/')."/{$this->largeCamelName}.php";
        $this->destinationFiles['validatorFiles']['validator'] = rtrim($this->destinationDirs['validatorDir'],'/')."/{$this->largeCamelName}/{$this->largeCamelName}Validator.php";
        $this->destinationFiles['validatorFiles']['notRequiredValidator'] = rtrim($this->destinationDirs['validatorDir'],'/')."/{$this->largeCamelName}/NotRequired{$this->largeCamelName}Validator.php";
        $this->destinationFiles['viewFiles']['vue'] = rtrim($this->destinationDirs['viewsDir'],'/')."/views/{$this->largeCamelName}.vue";
        $this->destinationFiles['viewFiles']['router'] = rtrim($this->destinationDirs['viewsDir'],'/')."/router/{$this->largeCamelName}.js";
        $this->destinationFiles['viewFiles']['api'] = rtrim($this->destinationDirs['viewsDir'],'/')."/apis/{$this->largeCamelName}.js";
    }

    /**
     * @throws ForeignKeyNotMatchException
     * @throws TableNotFoundException
     */
    private function checkTable()
    {
        $tableName = $this->input->getOption('table');
        if (empty($tableName))
        {
            throw new TableNotFoundException("Table name can not be empty!");
        }

        list($this->tableFullName,$this->tableName,$this->smallCamelName,$this->largeCamelName) = $this->checkTableExists($tableName);

        $this->checkRelationTables();
    }

    private function checkRelationTables()
    {
        $relationTables = $this->input->getOption('relation');
        $relationKeys = $this->input->getOption('relationKey');
        $relationPrimaryKeys = $this->input->getOption('relationPrimaryKey');

        if ($relationKeys)
        {
            $relationKeys = explode(',',str_replace('，',',',$relationKeys));
        }
        if ($relationPrimaryKeys)
        {
            $relationPrimaryKeys = explode(',',str_replace('，',',',$relationPrimaryKeys));
        }

        if ($relationTables)
        {
            $relationTables = explode(',',str_replace('，',',',$relationTables));
            if (is_array($relationKeys) && count($relationKeys) != count($relationTables))
            {
                throw new ForeignKeyNotMatchException('The foreign keys length must be the same as relation tables\'s length!');
            }

            if (is_array($relationPrimaryKeys) && count($relationPrimaryKeys) != count($relationTables))
            {
                throw new ForeignKeyNotMatchException('The foreign primary keys length must be the same as relation tables\'s length!');
            }

            foreach ($relationTables as $k => $relationTable)
            {
                list($tableFullName,$tableName,$smallCamelName,$largeCamelName) = $this->checkTableExists($relationTable);
                $this->foreignTables[$relationTable] = [
                    'tableFullName'=>$tableFullName,
                    'tableName'=>$tableName,
                    'smallCamelName'=>$smallCamelName,
                    'largeCamelName'=>$largeCamelName
                ];

                if (is_array($relationKeys))
                {
                    $this->foreignTables[$relationTable]['relationKey'] = $relationKeys[$k];
                }
                else
                {
                    $this->foreignTables[$relationTable]['relationKey'] = $this->foreignTables[$relationTable]['tableName'].'_id';
                }

                if (is_array($relationPrimaryKeys))
                {
                    $this->foreignTables[$relationTable]['relationPrimaryKey'] = $relationPrimaryKeys[$k];
                }
                else
                {
                    $this->foreignTables[$relationTable]['relationPrimaryKey'] = 'id';
                }
            }
        }
    }

    private function checkTableExists(string $tableName)
    {
        list($tableFullName,$tableName) = $this->getTableInfo($tableName);
        if (!$tableFullName) {
            $tableFullName = $this->db->getPrefix() . $tableName;
            list($tableFullName,$tableName) = $this->getTableInfo($tableFullName);
            if (!$tableFullName) {
                throw new TableNotFoundException("Table name <{$tableName}> not found!");
            }
        }
        $camelName = mackToCamelCase($tableName);
        return [$tableFullName,$tableName,$camelName,ucfirst($camelName)];
    }

    private function getTableInfo($tableName) : array
    {
        $tableName = trim($tableName);
        $info = Db::select("SHOW TABLE STATUS LIKE '{$tableName}'", [], TRUE);
        if ($info)
        {
            return [$tableName,mackLtrimPlus($tableName,$this->db->getPrefix())];
        }
        else
        {
            return [null,$tableName];
        }
    }

    private function setTableInfo()
    {
        $this->tableDefinitionObject = \bean('tableDefinitionObject');
        $firstRow = $this->originData[0];
        $this->tableDefinitionObject->setTableCatalog($firstRow['TABLE_CATALOG']);
        $this->tableDefinitionObject->setTableSchema($firstRow['TABLE_SCHEMA']);
        $this->tableDefinitionObject->setTableName($this->tableName);
    }

    private function initColumnStructs()
    {
        $this->columns = $this->parseOriginDataToColumnStructs($this->originData);
        foreach ($this->foreignOriginData as $tableName => $foreignOriginDatum)
        {
            $this->foreignColums[$tableName] = $this->parseOriginDataToColumnStructs($foreignOriginDatum);
        }
    }
}
