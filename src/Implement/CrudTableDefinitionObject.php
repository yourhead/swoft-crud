<?php


namespace Swoft\Crud\Implement;


use Swoft\Crud\Contract\CrudColumnDefinitionObjectInterface;
use Swoft\Crud\Contract\CrudTableDefinitionObjectInterface;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class CrudTableDefinitionObject
 * @Bean(name="tableDefinitionObject")
 */
class CrudTableDefinitionObject implements CrudTableDefinitionObjectInterface
{
    /**
     * @var string $tableCatalog
     */
    private $tableCatalog;

    /**
     * @var string $tableSchema
     */
    private $tableSchema;

    /**
     * @var string $tableName
     */
    private $tableName;

    /**
     * @var CrudColumnDefinitionObjectInterface $primaryKey
     */
    private $primaryKey;

    /**
     * @var array $definitions
     */
    private $definitions;

    /**
     * 获取数据表登记目录
     * @return string
     */
    public function getTableCatalog()
    {
        return $this->tableCatalog;
    }

    /**
     * 获取数据表所属的数据库名
     * @return string
     */
    public function getTableSchema()
    {
        return $this->tableSchema;
    }

    /**
     * 获取表名
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableCatalog
     */
    public function setTableCatalog(string $tableCatalog): void
    {
        $this->tableCatalog = $tableCatalog;
    }

    /**
     * @param string $tableSchema
     */
    public function setTableSchema(string $tableSchema): void
    {
        $this->tableSchema = $tableSchema;
    }

    /**
     * @param string $tableName
     */
    public function setTableName(string $tableName): void
    {
        $this->tableName = $tableName;
    }

    /**
     * @return CrudColumnDefinitionObjectInterface
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function initByColumnDefinition(array $definitions)
    {
        $this->definitions = $definitions;
        $this->setPrimaryKey();
    }

    private function setPrimaryKey()
    {
        /** @var CrudColumnDefinitionObjectInterface $definition */
        foreach ($this->definitions as $definition)
        {
            if ($definition->isPrimaryKey())
            {
                $this->primaryKey = $definition;
                break;
            }
        }
    }
}
