<?php

use Swoft\Db\Database;
use Swoft\Db\Pool;

return [
    'db'                 => [
        'class'    => Database::class,
        'dsn'      => 'mysql:dbname=fastadmin;host=127.0.0.1',
        'username' => 'root',
        'password' => 'Vtime123!@#',
        'charset'  => 'utf8mb4',
        'prefix'   => 'fa_',
        'options'  => [
            PDO::ATTR_CASE => PDO::CASE_NATURAL
        ],
        'config'   => [
            'collation' => 'utf8mb4_unicode_ci',
            'strict'    => true,
            'timezone'  => '+8:00',
            'modes'     => 'NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES',
            'fetchMode' => PDO::FETCH_ASSOC
        ]
    ],
    'db.pool'   =>  [
        'class'     => Pool::class,
        'database'  => bean('db'),
    ],
];
