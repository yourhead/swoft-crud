<?php


namespace Swoft\Crud\Validator;

use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * Class PaginateValidator
 * @Validator(name="PaginateValidator")
 */
class PaginateValidator
{
    /**
     * @IsInt()
     * @var int
     */
    protected $size;

    /**
     * @IsInt()
     * @var int
     */
    protected $page;
}
