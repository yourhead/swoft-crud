<?php


namespace Swoft\Crud\Contract;


interface CrudTableDefinitionObjectInterface
{
    /**
     * 获取数据表登记目录
     * @return string
     */
    public function getTableCatalog();

    /**
     * 获取数据表所属的数据库名
     * @return string
     */
    public function getTableSchema();

    /**
     * 获取表名
     * @return string
     */
    public function getTableName();

    /**
     * @return CrudColumnDefinitionObjectInterface
     */
    public function getPrimaryKey();
}
